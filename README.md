# Django Swift Proxy

## Purpose

[Django](https://www.djangoproject.com/) application that acts mostly
like an authentication proxy for
[OpenStack Swift](http://docs.openstack.org/api/openstack-object-storage/1.0/content/)
and adds some basic authorization features. This module can be used for CDN building,
since it's able to resfresh content on caching nodes using HTTP PURGE method.

## Features

* Performs [authentication](http://docs.openstack.org/api/openstack-object-storage/1.0/content/authentication-object-dev-guide.html) against Swift using stored username and key.
* Stores keys and tokens encrypted with AES (using [django-fields](https://github.com/svetlyak40wt/django-fields))
* Administrator can allow anonymous readonly access to a specific Container for a specific list of IP addresses/networks (`0.0.0.0/0` is the whole Internet)
* Administrator can give readonly or readwrite access to specific containers to Django users or groups
* Service users can use either Django authentication (built-in or some third-party middleware) or Basic HTTP Authentication
* Basic HTTP Authentication is intentionally supported for secure (HTTPS) connections only
* Multiple Swift servers or credentials could be used, the same Swift container could be exposed under different URLs and permissions
* Most part of [Swift API for Storage Objects](http://docs.openstack.org/api/openstack-object-storage/1.0/content/storage-object-services.html) is supported
* Conditional and Range GET requests are supported
* The proxy can send PURGE requests to a number of proxies to remove uploaded, updated or deleted object from caches

## Requirements

* [django-fields](https://github.com/svetlyak40wt/django-fields)
* [django-guardian](https://github.com/lukaszb/django-guardian)
* [django-appconf](https://github.com/jezdez/django-appconf)
* [ipaddr](https://code.google.com/p/ipaddr-py/)
* [python-swiftclient](https://github.com/openstack/python-swiftclient)


## Installation

`$ pip install https://bitbucket.org/svitkovskiy/django-swift-proxy.git`

All dependencies should be picked up from `setup.py` by PIP.

## Configuration

Add `'djswiftproxy'` to your `INSTALLED_APPS` in Django `settings.py`.

Add the following line to your `urls.py`:

    url(r'^storage/', include('djswiftproxy.urls')),

### Settings

The following settings are available:

**`DSP_DEFAULT_RETRIES`** - default number of retries when non-fatal errors happen (default: 5)

**`DSP_DEFAULT_BACKOFF`** - default base interval between retries, seconds (defailt: 0.3)

**`DSP_DEFAULT_CREDS_TTL`** - default credentials retention period, seconds. After this period of time, Swift access key is discarded and the client is reauthenticated (default: 3600)

**`DSP_RESP_CHUNK_SIZE`** - Swift reponse chunk size, bytes. If set to `None`, the whole Swift response will be put to memory and then served from there (default: 4096)

**`DSP_PUT_CHUNK_SIZE`** - When an object is uploaded, chunks of this size in bytes is uploaded at once (default: 65536)


`DSP_DEFAULT_RETRIES`, `DSP_DEFAULT_BACKOFF` and `DSP_DEFAULT_CREDS_TTL` could be redefined for every individual `SwiftServer` instance.
