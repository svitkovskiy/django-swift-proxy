from appconf import AppConf


class DjswiftproxyAppConf(AppConf):

    DEFAULT_RETRIES = 5
    DEFAULT_BACKOFF = 0.3
    DEFAULT_CREDS_TTL = 3600
    RESP_CHUNK_SIZE = 4096
    PUT_CHUNK_SIZE = 65536

    class Meta:
        # All settings will have "DSP_" prefix
        prefix = 'dsp'
