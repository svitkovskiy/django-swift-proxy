import logging
import httplib
import json

from django.http import HttpResponse

log = logging.getLogger(__name__)


class HttpResponseUnautorized(HttpResponse):
    status_code = 401

    def __init__(self,  *args, **kwargs):
        auth_type = kwargs.pop('auth_type', 'Basic')
        if auth_type.lower() == 'basic':
            auth_basic_realm = kwargs.pop('auth_basic_realm', 'Restricted')
        super(HttpResponseUnautorized, self).__init__(*args, **kwargs)
        if auth_type.lower() == 'basic':
            self['WWW-Authenticate'] = 'Basic realm=%s' % auth_basic_realm


def send_purge(address, uri, method='PURGE', host_header=None, https=False):
    cls = httplib.HTTPSConnection if https else httplib.HTTPConnection
    headers = {'Host': host_header} if host_header else {}
    connection = cls(address[0], address[1], timeout=2)
    connection.request(method, uri, headers=headers)
    response = connection.getresponse()
    status = int(response.status)
    if (status < 200 or status > 299) and not status == 404:
        raise ValueError("Bad status code from %s:%i : %i" %
                         (address[0], address[1], status))


def serialise_dict(request, dict_data):
    if 'cacti' in request.GET:
        # Make a cacti-acepted string: <key1>:<value1> <key2>:<value2>...
        l = ' '.join(map(lambda (k, v): '%s:%s' % (
            k, v.replace(' ', '_').replace(':', '-')), dict_data.items()))
        return HttpResponse(l, content_type='text/plain')
    # Return JSON by default
    return HttpResponse(json.dumps(dict_data), content_type='application/json')
