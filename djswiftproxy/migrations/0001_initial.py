# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SwiftServer'
        db.create_table(u'djswiftproxy_swiftserver', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('authurl', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('user', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('key', self.gf('django_fields.fields.EncryptedCharField')(max_length=549, cipher='AES')),
            ('retries', self.gf('django.db.models.fields.IntegerField')(default=5)),
            ('preauthurl', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('preauthtoken', self.gf('django_fields.fields.EncryptedCharField')(max_length=549, null=True, cipher='AES', blank=True)),
            ('snet', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('default_backoff', self.gf('django.db.models.fields.FloatField')(default=0.3)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=2048, blank=True)),
            ('creds_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('creds_ttl', self.gf('django.db.models.fields.IntegerField')(default=3600)),
        ))
        db.send_create_signal(u'djswiftproxy', ['SwiftServer'])

        # Adding model 'SwiftContainer'
        db.create_table(u'djswiftproxy_swiftcontainer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('server', self.gf('django.db.models.fields.related.ForeignKey')(related_name='containers', to=orm['djswiftproxy.SwiftServer'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=2048, blank=True)),
            ('proxyprefix', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'djswiftproxy', ['SwiftContainer'])

        # Adding model 'SwiftContainerUserObjectPermission'
        db.create_table(u'djswiftproxy_swiftcontaineruserobjectpermission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('permission', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Permission'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('content_object', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['djswiftproxy.SwiftContainer'])),
        ))
        db.send_create_signal(u'djswiftproxy', ['SwiftContainerUserObjectPermission'])

        # Adding unique constraint on 'SwiftContainerUserObjectPermission', fields ['user', 'permission', 'content_object']
        db.create_unique(u'djswiftproxy_swiftcontaineruserobjectpermission', ['user_id', 'permission_id', 'content_object_id'])

        # Adding model 'SwiftContainerGroupObjectPermission'
        db.create_table(u'djswiftproxy_swiftcontainergroupobjectpermission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('permission', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Permission'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'])),
            ('content_object', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['djswiftproxy.SwiftContainer'])),
        ))
        db.send_create_signal(u'djswiftproxy', ['SwiftContainerGroupObjectPermission'])

        # Adding unique constraint on 'SwiftContainerGroupObjectPermission', fields ['group', 'permission', 'content_object']
        db.create_unique(u'djswiftproxy_swiftcontainergroupobjectpermission', ['group_id', 'permission_id', 'content_object_id'])

        # Adding model 'SwiftContainerAnonNet'
        db.create_table(u'djswiftproxy_swiftcontaineranonnet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('container', self.gf('django.db.models.fields.related.ForeignKey')(related_name='anon_nets', to=orm['djswiftproxy.SwiftContainer'])),
            ('net', self.gf('djswiftproxy.fields.IPNetworkField')()),
        ))
        db.send_create_signal(u'djswiftproxy', ['SwiftContainerAnonNet'])

        # Adding model 'AcceleratorProxy'
        db.create_table(u'djswiftproxy_acceleratorproxy', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('container', self.gf('django.db.models.fields.related.ForeignKey')(related_name='accelerators', to=orm['djswiftproxy.SwiftContainer'])),
            ('ip', self.gf('djswiftproxy.fields.IPAddressField')()),
            ('port', self.gf('django.db.models.fields.IntegerField')(default=80)),
            ('https', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'djswiftproxy', ['AcceleratorProxy'])


    def backwards(self, orm):
        # Removing unique constraint on 'SwiftContainerGroupObjectPermission', fields ['group', 'permission', 'content_object']
        db.delete_unique(u'djswiftproxy_swiftcontainergroupobjectpermission', ['group_id', 'permission_id', 'content_object_id'])

        # Removing unique constraint on 'SwiftContainerUserObjectPermission', fields ['user', 'permission', 'content_object']
        db.delete_unique(u'djswiftproxy_swiftcontaineruserobjectpermission', ['user_id', 'permission_id', 'content_object_id'])

        # Deleting model 'SwiftServer'
        db.delete_table(u'djswiftproxy_swiftserver')

        # Deleting model 'SwiftContainer'
        db.delete_table(u'djswiftproxy_swiftcontainer')

        # Deleting model 'SwiftContainerUserObjectPermission'
        db.delete_table(u'djswiftproxy_swiftcontaineruserobjectpermission')

        # Deleting model 'SwiftContainerGroupObjectPermission'
        db.delete_table(u'djswiftproxy_swiftcontainergroupobjectpermission')

        # Deleting model 'SwiftContainerAnonNet'
        db.delete_table(u'djswiftproxy_swiftcontaineranonnet')

        # Deleting model 'AcceleratorProxy'
        db.delete_table(u'djswiftproxy_acceleratorproxy')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'djswiftproxy.acceleratorproxy': {
            'Meta': {'object_name': 'AcceleratorProxy'},
            'container': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'accelerators'", 'to': u"orm['djswiftproxy.SwiftContainer']"}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'https': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('djswiftproxy.fields.IPAddressField', [], {}),
            'port': ('django.db.models.fields.IntegerField', [], {'default': '80'})
        },
        u'djswiftproxy.swiftcontainer': {
            'Meta': {'object_name': 'SwiftContainer'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2048', 'blank': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'proxyprefix': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'containers'", 'to': u"orm['djswiftproxy.SwiftServer']"}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'djswiftproxy.swiftcontaineranonnet': {
            'Meta': {'object_name': 'SwiftContainerAnonNet'},
            'container': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'anon_nets'", 'to': u"orm['djswiftproxy.SwiftContainer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'net': ('djswiftproxy.fields.IPNetworkField', [], {})
        },
        u'djswiftproxy.swiftcontainergroupobjectpermission': {
            'Meta': {'unique_together': "([u'group', u'permission', u'content_object'],)", 'object_name': 'SwiftContainerGroupObjectPermission'},
            'content_object': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['djswiftproxy.SwiftContainer']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Permission']"})
        },
        u'djswiftproxy.swiftcontaineruserobjectpermission': {
            'Meta': {'unique_together': "([u'user', u'permission', u'content_object'],)", 'object_name': 'SwiftContainerUserObjectPermission'},
            'content_object': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['djswiftproxy.SwiftContainer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Permission']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'djswiftproxy.swiftserver': {
            'Meta': {'object_name': 'SwiftServer'},
            'authurl': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'creds_ttl': ('django.db.models.fields.IntegerField', [], {'default': '3600'}),
            'creds_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'default_backoff': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2048', 'blank': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django_fields.fields.EncryptedCharField', [], {'max_length': '549', 'cipher': "'AES'"}),
            'preauthtoken': ('django_fields.fields.EncryptedCharField', [], {'max_length': '549', 'null': 'True', 'cipher': "'AES'", 'blank': 'True'}),
            'preauthurl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'retries': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'snet': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['djswiftproxy']