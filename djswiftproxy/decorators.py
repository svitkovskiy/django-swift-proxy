import base64
import logging

from django.contrib.auth import authenticate
from django.conf import settings

from .http import HttpResponseUnautorized

log = logging.getLogger(__name__)


def basic_http_auth(func, require_secure=True):
    def inner(request, *args, **kwargs):
        # Skip if there's no header of the user is authenticated already
        if not 'HTTP_AUTHORIZATION' in request.META or \
            request.user.is_authenticated():
            return func(request, *args, **kwargs)
        # If request isn't secure enough (is not HTTPS)
        is_secure = not require_secure or request.is_secure() or settings.DEBUG
        if not is_secure:
            return HttpResponseUnautorized()
        # Parse the header
        auth = request.META['HTTP_AUTHORIZATION'].split()
        if not len(auth) == 2 or not auth[0].lower() == 'basic':
            return HttpResponseUnautorized()
        try:
            username, password = base64.b64decode(auth[1]).split(':')
        except:
            return HttpResponseUnautorized()
        # Authenticate the used with basic credentials
        user = authenticate(username=username, password=password)
        if not user or not user.is_active:
            return HttpResponseUnautorized()
        # Intentionaly not using login() here because it creates
        # a session, while we don't need one
        request.user = user
        return func(request, *args, **kwargs)
    return inner
