from swiftclient.client import Connection as SwiftConnection


class DjangoSwiftConnection(SwiftConnection):

    def __init__(self, *args, **kwargs):
        self._model = kwargs.pop('model', None)
        super(DjangoSwiftConnection, self).__init__(*args, **kwargs)

    def get_auth(self, *args, **kwargs):
        url, token = super(DjangoSwiftConnection, self).get_auth(*args,
                                                                 **kwargs)
        if self._model:
            self._model.update_creds(url, token)
        return url, token
