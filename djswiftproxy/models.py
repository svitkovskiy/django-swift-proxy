from urlparse import urlparse
from datetime import timedelta
import logging

from django.db import models
from django.conf import settings
from django.utils.timezone import now
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.utils.functional import Promise

from django_fields.fields import EncryptedCharField
from guardian.models import UserObjectPermissionBase
from guardian.models import GroupObjectPermissionBase

from .fields import IPNetworkField, IPNetworkQuerySet, IPAddressField
from .swift import DjangoSwiftConnection
from .settings import DjswiftproxyAppConf

try:
    from south.signals import post_migrate as ct_sync_signal
except ImportError:
    from django.db.models.signals import post_syncdb as ct_sync_signal

log = logging.getLogger(__name__)


class SwiftServer(models.Model):
    """OpenStack Obejct Storage server (swift) representation"""
    authurl = models.CharField(max_length=255)
    user = models.CharField(max_length=255)
    key = EncryptedCharField(max_length=255)
    retries = models.IntegerField(default=settings.DSP_DEFAULT_RETRIES)
    preauthurl = models.CharField(max_length=255, null=True, blank=True)
    preauthtoken = EncryptedCharField(max_length=255, null=True, blank=True)
    snet = models.BooleanField(default=False)
    default_backoff = models.FloatField(default=settings.DSP_DEFAULT_BACKOFF,
                                        help_text="Base pause before retries")
    enabled = models.BooleanField(default=True)
    description = models.TextField(max_length=2048, blank=True)
    creds_updated = models.DateTimeField(auto_now_add=True)
    creds_ttl = models.IntegerField(default=settings.DSP_DEFAULT_CREDS_TTL,
                                    help_text="Reauthentication period")

    class Meta:
        verbose_name = "SWIFT Server"
        verbose_name_plural = "SWIFT Servers"
        permissions = (
            ('see_stats', 'See SWIFT stats'),
        )

    def update_creds(self, preauthurl, preauthtoken):
        no_changes = preauthurl == self.preauthurl and \
            preauthtoken == self.preauthtoken
        if no_changes and not self.creds_expired:
            return
        log.info("Updating credentials for %s" % self)
        self.preauthurl = preauthurl
        self.preauthtoken = preauthtoken
        self.creds_updated = now()
        self.save()

    @property
    def creds_expired(self):
        return now() > (self.creds_updated + timedelta(seconds=self.creds_ttl))

    @property
    def connection(self):
        if hasattr(self, '_connection'):
            return self._connection
        preauthurl = None if self.creds_expired else self.preauthurl
        preauthtoken = None if self.creds_expired else self.preauthtoken
        self._connection = DjangoSwiftConnection(authurl=self.authurl,
                                                 user=self.user, key=self.key,
                                                 retries=self.retries,
                                                 preauthurl=preauthurl,
                                                 preauthtoken=preauthtoken,
                                                 model=self)
        return self._connection

    @property
    def stats_uri(self):
        return reverse('server_stats', args=(self.id,))

    def __unicode__(self):
        return "#%s %s" % (self.pk, '://'.join(urlparse(self.authurl)[0:2]))


class SwiftContainer(models.Model):
    """Individual slug/mountpoint mapping"""
    slug = models.CharField(max_length=255, unique=True)
    server = models.ForeignKey('SwiftServer', related_name='containers')
    name = models.CharField(max_length=255, help_text="Swift container name")
    enabled = models.BooleanField(default=True)
    description = models.TextField(max_length=2048, blank=True)
    proxyprefix = models.CharField(max_length=255, blank=True, default=None,
                                   null=True, verbose_name="Proxy prefix",
                                   help_text="HTTP URL prefix"
                                   " to PURGE from proxies:"
                                   " [[http:]//host]/prefix/")

    class Meta:
        permissions = (
            ('ro', 'R/O access'),
            ('rw', 'R/W access'),
            ('ls', 'List access'),
        )
        verbose_name = "SWIFT Container"
        verbose_name_plural = "SWIFT Containers"

    def get_absolute_url(self):
        return reverse('swift_proxy', args=[self.slug, ''])
    get_absolute_url.short_description = "Prefix URI"

    @property
    def stats_uri(self):
        return reverse('container_stats', args=(self.slug,))

    def __unicode__(self):
        return "%s -> %s/%s" % (self.slug, self.server, self.name)


class SwiftContainerUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(SwiftContainer)

    def __unicode__(self):
        return "%s: %s" % (self.user.username, self.permission.codename)


class SwiftContainerGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(SwiftContainer)

    def __unicode__(self):
        return "%s: %s" % (self.group.name, self.permission.codename)


class SwiftContainerAnonNet(models.Model):
    """Allow anonymous GET/HEAD for these networks"""
    objects = IPNetworkQuerySet.as_manager()
    container = models.ForeignKey('SwiftContainer',
                                  related_name='anon_nets')
    net = IPNetworkField()

    class Meta:
        verbose_name = "Anonymous R/O network"
        verbose_name_plural = "Anonymous R/O networks"

    def __unicode__(self):
        return "Allow Anonymous R/O from %s" % (self.net)


class AcceleratorProxy(models.Model):
    """Accelerator proxy to PURGE objects from in case of POST/PUT/DELETE"""
    container = models.ForeignKey('SwiftContainer',
                                  related_name='accelerators')
    ip = IPAddressField()
    port = models.IntegerField(default=80)
    https = models.BooleanField(default=False)
    enabled = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Accelerator proxy"
        verbose_name_plural = "Accelerator proxies"

    def __unicode__(self):
        return "Proxy %s://%s:%i" % ('https' if self.https else 'http',
                                     self.ip, self.port)


def update_contenttypes_names(**kwargs):
    """Synchronise content types' names"""
    for c in ContentType.objects.all():
        cl = c.model_class()
        if cl in (SwiftServer, SwiftContainer, SwiftContainerAnonNet,
                  AcceleratorProxy):
            new_name = cl._meta.verbose_name
            if c.name != new_name:
                log.info("Updating ContentType's name: '%s' -> '%s'", c.name,
                         new_name)
                c.name = new_name
                c.save()

ct_sync_signal.connect(update_contenttypes_names)
