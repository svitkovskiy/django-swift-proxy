from django.conf.urls import patterns, url


urlpatterns = patterns('djswiftproxy.views',
                       url(r'^_stats/server/(?P<server_id>[0-9]+)/$',
                           'server_stats', name='server_stats'),
                       url(r'^_stats/container/(?P<slug>.+)/$',
                           'container_stats', name='container_stats'),
                       url(r'^(?P<slug>[a-zA-Z0-9-_]+)/$',
                           'list_container_view', name='list_container'),
                       url(r'^(?P<slug>[a-zA-Z0-9-_]+)/(?P<object_uri>.*)$',
                           'proxy_view', name='swift_proxy'),
                       )
