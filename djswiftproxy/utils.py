import logging

from swiftclient.client import ClientException


class SwiftFilter(logging.Filter):
    """
    This loggig filter hides exceptions that are 'normal', i.e. when swift
    responds with 304, 412 or 404 codes

    LOGGING = {
    ...
        'filters': {
            ...
            'swiftclient': {
                '()': 'djswiftproxy.utils.SwiftFilter',
            },
        },
        ...
        'loggers': {
            ...
            'swiftclient': {
                'level': 'INFO',
                'filters': ['swiftclient', ],
            }
        }
    }


    """

    def filter(self, record):
        e = record.exc_info[1] if record.exc_info else None
        if isinstance(e, ClientException) and e.http_status in (304, 412, 404):
            return False
        return True
