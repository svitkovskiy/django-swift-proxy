import sys
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from djswiftproxy.models import SwiftContainer

LISTING_LIMIT = 500

## {'x-cdn-ttl': '1440', 'content-length': '7829',
## 'x-container-object-count': '41', 'accept-ranges': 'bytes',
## 'x-container-read': '.r:*', 'x-container-bytes-used': '6371',
## 'x-trans-id': 'tx394815789994b7e8c8969f7881e447c',
## 'date': 'Fri, 16 Aug 2013 11:50:21 GMT',
## 'content-type': 'application/json; charset=utf-8'}
##
## {'bytes': 0, 'last_modified': '2013-07-25T13:40:33.492960',
## 'hash': 'd41d8cd98f00b204e9800998ecf8427e',
## 'name': 'something/something',
## 'content_type': 'application/directory'}

def print_info(o, info):
    for k, v in info.items():
        o.write("%s: %s" % (k, v))
    o.write('\n')

def print_listing(o, listing, verbose=False):
    fmt = "%(content_type)22s %(bytes)10i %(last_modified)s %(name)s" if \
        verbose else "%(name)s"
    for obj in listing:
        o.write(fmt % obj)


class Command(BaseCommand):
    args = "container_name"
    help = "Lists containers' contents"
    option_list = BaseCommand.option_list + (
        make_option('--verbose',
            action='store_true',
            dest='verbose_list',
            default=False,
            help='Verbose listing'),
        make_option('--dump-metadata',
            action='store_true',
            dest='print_info',
            default=False,
            help='Output container metadata along with listing'),
        make_option('--listing-limit',
            type='int',
            dest='listing_limit',
            default=LISTING_LIMIT,
            help='Limit listing iterations by this number of objects'
            ' (affects performance). Default: %default'),
        make_option('--marker',
            dest='marker',
            default=None,
            help='Start listing from the marker (object name)'),
        make_option('--end-marker',
            dest='end_marker',
            default=None,
            help='End the listing with the marker (object name)'),
        make_option('--path',
            dest='path',
            default=None,
            help='List objects under the "directory" specified'),
    )


    def handle(self, *args, **options):
        try:
            slug = args[0]
        except IndexError:
            raise CommandError("Please, specify container slug")
        try:
            container = SwiftContainer.objects.get(slug=slug)
        except SwiftContainer.DoesNotExist:
            raise CommandError("Container with slug %s does not exist" % slug)
        connection = container.server.connection
        info, listing = connection.get_container(
            container.name, limit=options['listing_limit'],
            marker=options['marker'], end_marker=options['end_marker'],
            path=options['path'])
        if options['print_info']:
            print_info(self.stdout, info)
        print_listing(self.stdout, listing, options['verbose_list'])
        while len(listing) == options['listing_limit']:
            listing = connection.get_container(
                container.name, limit=options['listing_limit'],
                marker=listing[-1]['name'], end_marker=options['end_marker'],
                path=options['path'])[1]
            print_listing(self.stdout, listing, options['verbose_list'])
