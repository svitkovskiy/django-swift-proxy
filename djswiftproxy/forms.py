from django import forms
from django.contrib.auth.models import Permission, User

from .models import (SwiftServer, SwiftContainer,
                     SwiftContainerUserObjectPermission,
                     SwiftContainerGroupObjectPermission)


class ServerForm(forms.ModelForm):
    class Meta:
        model = SwiftServer
        widgets = {
            'default_backoff': forms.TextInput(attrs={'size': 3}),
            'retries': forms.TextInput(attrs={'size': 2}),
            'creds_ttl': forms.TextInput(attrs={'size': 4}),
            'authurl': forms.TextInput(attrs={'size': 50}),
            'user': forms.TextInput(attrs={'size': 25}),
            'key': forms.PasswordInput(attrs={'size': 60}, render_value=True),
            'preauthurl': forms.TextInput(attrs={'size': 100}),
            'preauthtoken': forms.PasswordInput(attrs={'size': 60},
                                                render_value=True),
            'description': forms.Textarea(attrs={'rows': 2, 'cols': 50}),
        }


class ContainerForm(forms.ModelForm):
    class Meta:
        model = SwiftContainer
        widgets = {
            'slug': forms.TextInput(attrs={'size': 20}),
            'name': forms.TextInput(attrs={'size': 20}),
            'proxyprefix': forms.TextInput(attrs={'size': 60}),
            'description': forms.Textarea(attrs={'rows': 2, 'cols': 50}),
        }


class PermChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class ContainerUserPermForm(forms.ModelForm):
    permission = PermChoiceField(
        queryset=Permission.objects.filter(
            content_type__app_label='djswiftproxy',
            content_type__model='swiftcontainer',
            codename__in=('ro', 'rw', 'ls'))
    )
    user = forms.ModelChoiceField(queryset=User.objects.filter(id__gt=0))

    class Meta:
        model = SwiftContainerUserObjectPermission


class ContainerGroupPermForm(forms.ModelForm):
    permission = PermChoiceField(
        queryset=Permission.objects.filter(
            content_type__app_label='djswiftproxy',
            content_type__model='swiftcontainer',
            codename__in=('ro', 'rw', 'ls'))
    )

    class Meta:
        model = SwiftContainerGroupObjectPermission
