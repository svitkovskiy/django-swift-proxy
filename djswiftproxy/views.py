import logging
import sys
from urlparse import urlparse
import json

from django.http import (HttpResponse, StreamingHttpResponse,
                         HttpResponseNotAllowed, HttpResponseServerError,
                         HttpResponseForbidden)
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.views.decorators.http import require_safe


from swiftclient.client import ClientException
from guardian.shortcuts import get_perms
from ipaddr import IPAddress

from .models import SwiftContainer, SwiftServer
from .decorators import basic_http_auth
from .http import HttpResponseUnautorized, send_purge, serialise_dict

log = logging.getLogger(__name__)


def is_authorised(request, container):
    """
    Check if this request is authorised for this storage.
    """
    user = getattr(request, 'user', None)
    ip = IPAddress(request.META['REMOTE_ADDR'])

    is_get = request.method in ('GET', 'HEAD')
    is_anonymous = not user or not user.is_active or user.is_anonymous() or \
        user.id in (getattr(settings, 'ANONYMOUS_USER_ID', -1), None)

    # Anonymous is allowed to GET/HEAD only from specified networks
    if is_anonymous:
        if is_get:
            for netobj in container.anon_nets.all():
                if ip in netobj.net:
                    return True
        return False

    permissions = get_perms(user, container)

    # Allow all acions if RW acl or GET/HEAD for RO
    if ('rw' in permissions) or (is_get and 'ro' in permissions):
        return True

    return False


def purge_from_accelerators(container, object_uri):
    """
    Send PURGE requests to all enabled Accelerators
    """
    if container.proxyprefix is None:
        return
    parsed = urlparse(container.proxyprefix)
    proxy_uri = parsed.path + object_uri
    proxy_host_header = parsed.netloc

    for accelerator in container.accelerators.filter(enabled=True):
        log.info("PURGE %s from %s", object_uri, accelerator)
        try:
            send_purge((str(accelerator.ip), accelerator.port), proxy_uri,
                       'PURGE', proxy_host_header, accelerator.https)
        except Exception:
            log.exception("PURGE of %s from %s has failed", object_uri,
                          accelerator)


def swift_request_headers(meta):
    """
    Parse interesting headers from Django request and prepare Swift request
    headers
    """
    result = {}
    for key, value in meta.items():
        key = key[5:] if key.startswith('HTTP_') else key
        if key == 'CONTENT_LENGTH' and not value:
            value = '0'
        if key in ('CONTENT_LENGTH', 'CONTENT_TYPE', 'RANGE', 'IF_MATCH',
                   'IF_NONE_MATCH', 'IF_MODIFIED_SINCE',
                   'IF_UNMODIFIED_SINCE', 'X_DELETE_AT',
                   'X_DELETE_AFTER', 'X_COPY_FROM') or key.startswith('X_OBJECT_META_'):
            result[key.replace('_', '-').title()] = value
    return result


def request_swift(request, container, uri):
    # Headers that (optionally) will be added to Swift request
    req_hdrs = swift_request_headers(request.META)
    method = request.method

    # Base response
    rsp_hdrs = {}
    response = HttpResponse()

    connection = container.server.connection

    # Perform different operations depending on request method
    if method == 'HEAD':
        rsp_hdrs = connection.head_object(container.name, uri)

    elif method == 'GET':
        rsp_hdrs, obj = connection.get_object(
            container.name, uri, resp_chunk_size=settings.DSP_RESP_CHUNK_SIZE,
            headers=req_hdrs)
        response = StreamingHttpResponse(streaming_content=obj)
        if 'content-range' in rsp_hdrs:
            response.status_code = 206

    elif method == 'PUT':
        rsp_hdrs['etag'] = connection.put_object(
            container.name, uri, contents=request, headers=req_hdrs,
            chunk_size=settings.DSP_PUT_CHUNK_SIZE)
        response.status_code = 201
        purge_from_accelerators(container, uri)

    elif method == 'POST':
        connection.post_object(container.name, uri, headers=req_hdrs)
        response.status_code = 204
        purge_from_accelerators(container, uri)

    elif method == 'DELETE':
        connection.delete_object(container.name, uri)
        response.status_code = 204
        purge_from_accelerators(container, uri)

    else:
        return HttpResponseNotAllowed(['GET', 'HEAD', 'POST', 'PUT', 'DELETE'])

    # Update response headers
    for header, value in rsp_hdrs.items():
        show_meta = header.startswith('x-object-meta-') \
            and request.user.is_authenticated()
        if show_meta or header in ('content-type', 'etag', 'content-length',
                                   'last-modified', 'content-range',
                                   'accept-ranges'):
            response[header.title()] = value

    return response


@csrf_exempt
@basic_http_auth
def proxy_view(request, slug, object_uri):
    """Main view that proxies HTTP requests to OpenStack storage"""

    # Deny directory listings
    if object_uri == '' or object_uri.endswith('/'):
        return HttpResponseForbidden("Directory listing is not allowed")

    # Get the container
    container = get_object_or_404(SwiftContainer, slug=slug)

    if not container.enabled or not container.server.enabled:
        return HttpResponse(status=410)

    if not is_authorised(request, container):
        return HttpResponseUnautorized()

    try:
        return request_swift(request, container, object_uri)
    except ClientException, e:
        if e.http_status in (304, 404, 412):
            sys.exc_clear()
            log.debug("Swift reports %s for %s", e.http_status, e.http_path)
            return HttpResponse(status=e.http_status)
        log.exception("Swift exception: %s", str(e))
        return HttpResponseServerError("Backend reports an error")


@basic_http_auth
@permission_required('djswiftproxy.see_stats', raise_exception=True)
def server_stats(request, server_id):
    server = get_object_or_404(SwiftServer, id=server_id)
    return serialise_dict(request, server.connection.head_account())


@basic_http_auth
@permission_required('djswiftproxy.see_stats', raise_exception=True)
def container_stats(request, slug):
    container = get_object_or_404(SwiftContainer, slug=slug)
    connection = container.server.connection
    return serialise_dict(request, connection.head_container(container.name))


@basic_http_auth
@require_safe
def list_container_view(request, slug):
    """
    Lists container's contents. Respects most of request parameters as
    documented in the official API guide:
    http://docs.openstack.org/api/openstack-object-storage/1.0/content/list-objects.html

    The only supported output format is JSON at the moment.
    """

    # Get the container
    container = get_object_or_404(SwiftContainer, slug=slug)

    if not container.enabled or not container.server.enabled:
        return HttpResponse(status=410)

    # For some weird reason, user.has_perm() doesn't work
    if not 'ls' in get_perms(request.user, container):
        return HttpResponseUnautorized()

    connection = container.server.connection

    # Building .get_container()'s kwargs from current GET
    kwargs = {}
    for param in ('marker', 'prefix', 'delimiter', 'end_marker', 'path'):
        if param in request.GET:
            kwargs[param] = request.GET[param]
    if 'limit' in request.GET:
        try:
            kwargs['limit'] = int(request.GET['limit'])
        except ValueError:
            pass

    rsp_hdrs, obj_list = connection.get_container(container.name, **kwargs)

    response = HttpResponse(json.dumps(obj_list), content_type='text/json')
    for h in ('x-container-object-count', 'x-container-bytes-used'):
        response[h.title()] = rsp_hdrs[h]
    return response
