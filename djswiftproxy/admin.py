from django.contrib import admin

from guardian.admin import GuardedModelAdmin

from .models import (SwiftServer, SwiftContainer, SwiftContainerAnonNet,
                     SwiftContainerUserObjectPermission,
                     SwiftContainerGroupObjectPermission, AcceleratorProxy)
from .forms import (ServerForm, ContainerForm, ContainerUserPermForm,
                    ContainerGroupPermForm)


class ServerAdmin(admin.ModelAdmin):

    def invalidate_creds(self, request, queryset):
        queryset.update(preauthurl=None, preauthtoken=None)

    invalidate_creds.short_description = "Invalidate auth token & storage URL"

    actions = (invalidate_creds, )
    list_display = ("authurl", "user", "description", "stats_uri", "enabled")
    form = ServerForm
    readonly_fields = ('preauthurl', )
    fieldsets = (
        ('Auth v1', {'fields': (('authurl', 'user', 'key'),)}),
        ('General', {'fields': (('default_backoff', 'retries', 'creds_ttl'),
                                ('enabled', 'description', 'snet'))}),
        ('Temporary credentials', {'fields': (('preauthurl', 'preauthtoken'),),
                                   'classes': ('collapse', )}),
    )


class ContainerAnonNetInline(admin.TabularInline):
    model = SwiftContainerAnonNet
    extra = 1


class AcceleratorProxyInline(admin.TabularInline):
    model = AcceleratorProxy
    extra = 1


class ContainerUserPermInline(admin.TabularInline):
    model = SwiftContainerUserObjectPermission
    form = ContainerUserPermForm
    extra = 1


class ContainerGroupPermInline(admin.TabularInline):
    model = SwiftContainerGroupObjectPermission
    form = ContainerGroupPermForm
    extra = 1


class ContainerAdmin(GuardedModelAdmin):
    inlines = (ContainerAnonNetInline, ContainerUserPermInline,
               ContainerGroupPermInline, AcceleratorProxyInline)
    fields = (('server', 'name'), ('slug', 'proxyprefix', 'description'),
              ('enabled',))
    list_display = ('name', 'slug', 'get_absolute_url', 'server',
                    'description', 'stats_uri', 'enabled')
    prepopulated_fields = {'slug': ('name',)}
    form = ContainerForm


admin.site.register(SwiftServer, ServerAdmin)
admin.site.register(SwiftContainer, ContainerAdmin)
