#!/usr/bin/env python

from setuptools import setup

setup(
    name='django-swift-proxy',
    version='0.7',
    description='Django Openstack Object Storage (Swift) proxy',
    author='Stanislav Vitkovskiy <stas.vitkovsky@gmail.com>',
    url='https://bitbucket.org/svitkovskiy/django-swift-proxy',
    packages=['djswiftproxy', ],
    install_requires=[
        'django-fields == 0.2.0',
        'django-guardian == 1.1.0',
        'django-appconf == 0.6',
        'ipaddr == 2.1.10',
        'python-swiftclient == 1.6.0',
    ],
)
